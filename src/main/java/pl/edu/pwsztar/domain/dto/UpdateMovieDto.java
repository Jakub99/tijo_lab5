package pl.edu.pwsztar.domain.dto;

public class UpdateMovieDto {
    private int id;
    private String title;
    private String image;
    private Integer year;
    private String videoId;

    public int getId() {
        return id;
    }

    public UpdateMovieDto() {
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
